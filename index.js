const express = require('express');
const mongoose = require('mongoose');
const dotenv=require(`dotenv`);
dotenv.config();
const app = express()	

const PORT = 3007;

app.use(express.json())
app.use(express.urlencoded({extended:true}))
// Mongoose connection
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true});

//DB connection notification
const db=mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open',() =>console.log(`Connected to Database`));


const userSchema=new mongoose.Schema(
{
	name:{
		type:String,
		required:[true,`Name is required`]
	},
	password:{
		type:String,
		required:[true,`Password is required`]
	},
}
)

const User=mongoose.model(`User`,userSchema)

app.post(`/signup`,(req, res)=> {
	User.findOne({name:req.body.name}).then((result,err) =>{

		if(result != null && result.name==req.body.name){
			return res.send(`User already registered`)
		} else {
			let newUser=new User({
				name:req.body.name,
				password:req.body.password
			})

			newUser.save().then((savedUser,err) =>{
				console.log(savedUser)
				if(savedUser){
					return res.send(savedUser)
				} else{
					return res.status(500).send(err)
				}
				})
			} 
		})
	})
app.listen(PORT, ()=>console.log(`Server connected at port ${PORT}`))